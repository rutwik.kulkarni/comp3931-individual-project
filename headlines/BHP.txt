08/04/21 Yahoo Is BHP Group Limited Sponsored (BHP) Outperforming Other Basic Materials Stocks This Year?
13/04/21 Yahoo Top-Rated Stocks: BHP Billiton Composite Rating Climbs To Near-Best 96
14/04/21 Yahoo Top Stock Reports for Abbott, salesforce & BHP
15/04/21 Yahoo The Zacks Analyst Blog Highlights: Abbott, Salesforce, BHP Group, Danaher and Zoom Video
16/04/21 Yahoo The Zacks Analyst Blog Highlights: BHP Group, Rio Tinto, Southern Copper and Freeport-McMoRan
21/04/21 MarketWatch BHP lifts copper guidance, iron-ore output falls
21/04/21 Yahoo Has BHP Billiton (BBL) Outpaced Other Basic Materials Stocks This Year?
22/04/21 Yahoo BHP (BHP) Crossed Above the 50-Day Moving Average: What That Means for Investors
26/04/21 Yahoo BHP (BHP) Just Overtook the 50-Day Moving Average
28/04/21 Yahoo How Many BHP Group (ASX:BHP) Shares Do Institutions Own?
05/05/21 SeekingAlpha BHP Group: Over 5% Yield And On An Uptrend
06/05/21 Yahoo BHP (BHP) Outpaces Stock Market Gains: What You Should Know
14/05/21 Yahoo BHP Group (BHP) Rides on High Iron Prices Amid High Costs
14/05/21 Yahoo BHP Remote Copper Operators Move Closer to Strike in Chile
25/05/21 Yahoo BHP Group (ASX:BHP) Stock Has Shown Weakness Lately But Financials Look Strong: Should Prospective Shareholders Make The Leap?
26/05/21 Yahoo BHP Chile Control Staff to Strike in Threat to Copper Supply
27/05/21 Yahoo BHP Is in Talks With Nutrien on Giant Potash Mine
27/05/21 SeekingAlpha BHP Group's Stock Valuations Are Not Excessive
01/06/21 Yahoo Is BHP Group (BHP) A Smart Long-Term Buy?
09/06/21 Yahoo BHP Spence copper mine workers in Chile to extend talks for two days - union
11/06/21 Yahoo BHP Dodges Chile Copper Strike With Eleventh-Hour Wage Deal
17/06/21 Yahoo BHP delays decision on Jansen potash project by a few months
17/06/21 Yahoo BHP Touts Potash Growth as Major Canadian Investment Looms
20/06/21 Yahoo Estimating The Intrinsic Value Of BHP Group (ASX:BHP)
22/06/21 Yahoo Potash partnership with BHP 'not our focus': Nutrien executive
24/06/21 Yahoo BHP looks to double base metals exploration budget, executive says
06/07/21 Yahoo Australian coal miners recapitalise, eye BHP assets and other M&A
16/07/21 Yahoo Did You Participate In Any Of BHP Group's (ASX:BHP) Fantastic 253% Return ?
18/07/21 SeekingAlpha BHP Billiton Can Hedge Against Inflation
19/07/21 Yahoo The Zacks Analyst Blog Highlights: BHP Group, Materion Corp, Tronox, MP Materials and Freeport-McMoRan
19/07/21 MarketWatch BHP 4Q Iron-Ore Output Down 2% On Year
19/07/21 MarketWatch BHP Expects Covid-19 to Remain a Challenge for Chile Mines -- Commodity Comment
20/07/21 Yahoo BHP Group (BBL): Are Hedge Funds Right About This Stock?
20/07/21 Yahoo BHP Reports 2% Rise in Iron Ore Production in Fiscal 2021
20/07/21 Yahoo BHP Is Said to Mull Oil Exit in Retreat From Fossil Fuels
21/07/21 MarketWatch BHP Inks Nickel Supply Deal With Tesla
22/07/21 Yahoo Tesla Strikes Deal With Top Miner BHP Over Nickel Supplies
22/07/21 Yahoo Tesla (TSLA) Locks Future Nickel Supply on Deal With BHP
23/07/21 Yahoo The Zacks Analyst Blog Highlights: BHP Group, Booking Holdings, CVS Health, Chipotle Mexican Grill and Exelon
23/07/21 Yahoo BHP signs conditional port services deal for Canada potash mine
27/07/21 Yahoo Were Hedge Funds Right About BHP Group (BHP)?
27/07/21 MarketWatch BHP Group Makes Offer for Noront for Equity Value of C$325 Mln
27/07/21 Yahoo Mining giant BHP offers to buy Canada's Noront Resources for $258 mln
27/07/21 Yahoo Mining giant BHP offers to buy Canada's Noront Resources for $258 million
27/07/21 Yahoo Is BHP Group Limited Sponsored (BHP) Outperforming Other Basic Materials Stocks This Year?
05/08/21 MarketWatch BHP Group Approves $800M Capital Expenditure For Oil Projects in U.S., Mexico
08/08/21 SeekingAlpha BHP Group: Positioning For The Decades To Come
10/08/21 Yahoo BHP Gets Closer to Wage Deal at World’s Largest Copper Mine
11/08/21 Yahoo Now Climate Activists Want BHP to Keep Hold of Its Fossil Fuels
11/08/21 Yahoo Great news for BHP Group (ASX:BHP): Insiders acquired stock in large numbers last year
12/08/21 Yahoo Has BHP Group Limited Sponsored (BHP) Outpaced Other Basic Materials Stocks This Year?
15/08/21 Yahoo Woodside in Advanced Talks to Buy A$20B BHP Oil Unit: AFR
16/08/21 DowJones BHP Considers Sale of $15 Billion-Valued Petroleum Business
16/08/21 Yahoo BHP Nears Oil and Gas Exit as Climate Scrutiny Intensifies
16/08/21 Yahoo BHP Falls on Worry Woodside Deal Isn’t a Clean Break From Fossil Fuels
16/08/21 MarketWatch BHP in Talks to Exit Oil and Gas Business
16/08/21 Yahoo BHP stock falls after confirming talks to sell oil business
16/08/21 Yahoo BHP vs. WPM: Which Stock Should Value Investors Buy Now?
16/08/21 MarketWatch ADRs Close Lower; China Finance Online, BHP Trade Actively
17/08/21 MarketWatch BHP FY21 Net Profit $11.30 Billion, Up 42% On-Year
17/08/21 MarketWatch BHP Expects Chinese Steel Output to Rise 5% in 2021 -- Commodity Comment
17/08/21 MarketWatch BHP Group's U.S.-listed shares diverge on plan to end dual structure
17/08/21 Yahoo BHP ADRs Fall on Plan to End Dual Listing, Woodside Deal Structure
17/08/21 DowJones Mining giant BHP chooses Sydney over London for main stock listing
17/08/21 Yahoo BHP reports best profit in nearly a decade, reshapes portfolio
17/08/21 Yahoo BHP Agrees to Exit Oil Business, Approves Giant New Potash Mine
17/08/21 DowJones BHP Bets on Lower-Carbon World With Petroleum Exit, $5.7 Billion Potash Project 
17/08/21 MarketWatch BHP keeps FTSE 100 in positive territory
17/08/21 Yahoo Australia's Woodside snares BHP oil, gas business in $28 billion merger
17/08/21 Yahoo FTSE 100 to Lose Second-Biggest Name as BHP Goes Home
17/08/21 Yahoo UK market set to lose major stock as BHP plans Australia shift
17/08/21 Yahoo Top investor issues warning to BHP Billiton as miner axes primary London listing
17/08/21 Yahoo BHP Group (BBL) Q4 2021 Earnings Call Transcript
18/08/21 Yahoo Australia's Woodside shares drop on BHP petroleum merger
18/08/21 Yahoo Woodside investors jittery on petroleum merger, BHP falls on listing change
18/08/21 DowJones BHP shares down following investor criticism of plan to ditch primary London listing
18/08/21 Yahoo Iron Ore’s Plunge Deepens as BHP Flags ‘Stern Test’ From China
18/08/21 Yahoo Why BHP is delisting from London
18/08/21 Yahoo BHP more than reverses gains as FTSE 100 trades lower
18/08/21 Yahoo BHP Group (BHP) Sees '21 Earnings Growth, Nears Petroleum Exit
20/08/21 Yahoo BHP has fired 48 workers for sexual harassment since 2019
20/08/21 Yahoo Canadian Nickel Miner Still Wants BHP Takeover, Shunning Forrest
24/08/21 Yahoo BHP Turns to Electric Pickups as Miners Seek Emissions Cuts
25/08/21 SeekingAlpha BHP Group - Miner's Dip Offers A Buying Opportunity
27/08/21 MarketWatch Woodside CEO Sees Plenty of Potential in Gulf of Mexico Under BHP Deal
30/08/21 MarketWatch Australian Billionaire Offers Premium to BHP Bid for Noront Resources
31/08/21 Yahoo BHP considers making COVID vaccinations mandatory at Australian sites
31/08/21 MarketWatch Australian Billionaire Offers Premium to BHP Bid for Noront Resources -- Update
31/08/21 Yahoo Mining Magnate Trumps BHP in Duel for Canadian Nickel Group
31/08/21 Yahoo Top Stock Reports for Adobe, Thermo Fisher & BHP Group
03/09/21 Yahoo Why BHP Group Stock Fell 15% in August
03/09/21 Yahoo Top Stock Reports for Verizon, CVS Health & BHP
07/09/21 Benzinga Caterpillar, BHP Partner On Zero Emission Mining Trucks: What Investors Should Know
08/09/21 Yahoo BHP Strikes Exploration Deal With Jeff Bezos-Backed Data Startup
08/09/21 Yahoo BHP signs partnership deal with billionaire-backed AI explorer KoBold
10/09/21 Yahoo Copper Supply Risks Ease With Codelco Wage Deal, New BHP Offer
12/09/21 SeekingAlpha Copper Is What Makes BHP A Future Mining King
14/09/21 Yahoo BHP handing unexpectedly small $3.9 billion clean-up tab to Woodside in oil merger
14/09/21 MarketWatch BHP Targets Net Zero for Suppliers, Shippers, But Not for Steelmaking Customers
14/09/21 Yahoo BHP aims to have curbed emissions from steelmaking customers by 2050
14/09/21 Yahoo BHP Spent Just Half a Day’s Profit Looking for Copper Last Year
14/09/21 MarketWatch BHP Says Memorandums of Understanding in Place With Importers for Jansen Output
21/09/21 Yahoo The Zacks Analyst Blog Highlights: Rio Tinto, BHP Group, Vale and Fortescue Metals Group
22/09/21 Yahoo Mining Magnate Parrying BHP Bid Expands Stake in Nickel Miner
23/09/21 Yahoo Why The 32% Return On Capital At BHP Group (ASX:BHP) Should Have Your Attention
24/09/21 SeekingAlpha BHP Group: Strike While The Iron Is Cold
04/10/21 Yahoo Why Shares of BHP Slumped in September
05/10/21 Yahoo BHP Discussing Congo Copper Deal in Shift of Strategy
05/10/21 Yahoo BHP Group (BHP) to Supply Nickel to EV Battery Manufacturer
07/10/21 Yahoo BHP to require COVID vaccinations at Australian sites
14/10/21 Yahoo Mood lukewarm on BHP climate change plan ahead of AGM
14/10/21 Yahoo BHP Overcomes Opposition From Some Investors to Climate Plan
18/10/21 Yahoo Australian Magnate Beats BHP on ‘Superior’ Offer for Canadian Nickel Miner
18/10/21 MarketWatch BHP First-Quarter Iron Ore, Copper Output Falls
18/10/21 MarketWatch BHP Plans to Creep Past Iron-Ore Shipment Target in Medium Term -- Commodity Comment
19/10/21 Yahoo BHP Group's (BHP) Q1 Iron Ore Output Dips on Planned Maintenance
19/10/21 Yahoo Could The BHP Group (ASX:BHP) Ownership Structure Tell Us Something Useful?
20/10/21 Yahoo BHP lifts takeover offer for Canadian nickel miner Noront to $339 million
20/10/21 Yahoo BHP Ups the Stakes in Battle With Billionaire For Nickel Group
21/10/21 SeekingAlpha BHP Group: Sell-Off Is Overdone, Huge Yield Available
21/10/21 Yahoo BHP studies the potential for using nickel mine waste to capture carbon
02/11/21 MarketWatch BHP Extends Noront Offer Deadline Amid Talks With Wyloo
05/11/21 DowJones BHP Nears $1.25 Billion Deal to Sell Controlling Stake in Two Coal Mines 
07/11/21 MarketWatch BHP to sell its 80% stake in BMC coal mines to Stanmore
07/11/21 DowJones BHP to Sell Controlling Stake in Two Coal Mines for Up to $1.35 Billion 
08/11/21 Yahoo BHP Extends Fossil Fuels Withdrawal With Met Coal Asset Sale
10/11/21 SeekingAlpha BHP Group: Get Paid >4% Waiting For Inflation
14/11/21 Yahoo Here's Why I Think BHP Group (ASX:BHP) Might Deserve Your Attention Today
22/11/21 MarketWatch BHP and Woodside Sign Binding Share Sale Agreement for Merger of BHP's Oil and Gas Portfolio With Woodside
22/11/21 MarketWatch BHP Advances Plans to Combine Oil, Gas Unit With Woodside -- Update
22/11/21 Yahoo BHP Group (BHP) & Woodside Set to Finalize Petroleum Merger
03/12/21 SeekingAlpha The Momentum Investor: Spotlight On BHP
08/12/21 MarketWatch BHP Still Backs Worker Vaccine Mandate, Will Proceed With Reconsultation
10/12/21 Yahoo Is BHP Group (ASX:BHP) Trading At A 33% Discount?
11/12/21 Yahoo Penn Series Developed International Index Fund Buys Prosus NV, BHP Group, Universal Music Group ...
12/12/21 Yahoo BHP ends talks with Wyloo over Noront takeover, ploughs ahead with bid
13/12/21 Yahoo UPDATE 1-Wyloo raises offer for nickel miner Noront after talks end with BHP
13/12/21 Yahoo Billionaire Forrest Reignites Battle With BHP for Nickel Miner
14/12/21 Yahoo BHP completes first blockchain copper concentrate trade with Minmetals
15/12/21 MarketWatch BHP Signs Energy Storage Deal in Its Plan to Go Carbon Neutral
16/12/21 Yahoo BHP Group (BHP) Petroleum Arm-Woodside Merger Gets ACCC Nod
17/12/21 Yahoo Fidelity Hastings Street Trust Buys Universal Music Group NV, SAP SE, Glencore PLC, Sells BHP ...
17/12/21 SeekingAlpha BHP Group: Healthy Merger Arbitrage Return With Minimal Risk
21/12/21 Yahoo UPDATE 1-Australia's BHP gets regulatory approvals to unify its corporate structure
21/12/21 Yahoo Canada's Noront goes with Wyloo's offer, gives BHP five days to match
21/12/21 Yahoo Fight for Canadian Nickel Miner Appears Over as BHP Bows Out
28/12/21 Yahoo T. Rowe Price Real Assets Fund, Inc. Buys BHP Group, Martin Marietta Materials Inc, Vulcan ...
06/01/22 Yahoo BHP Cuts More Jobs at Water-Stressed Chilean Copper Mine
10/01/22 MarketWatch BHP to Invest Up to $90 Million in Kabanga Nickel
11/01/22 Yahoo Centre Asset Management, LLC Buys BHP Group, Intel Corp, Coterra Energy Inc, Sells Meta ...
12/01/22 Yahoo Freeport-McMoRan Leads Mining Stock Rally; BHP Breaks Out
16/01/22 Yahoo BHP Group joins Australia's big miners in electric trains purchase to transport iron ore
18/01/22 Yahoo BHP Revives Appetite for Deals With Biggest Rivals in Sights
18/01/22 MarketWatch BHP 1H Production Lower on Weak Copper, Met Coal Output
18/01/22 MarketWatch BHP Metallurgical Coal Operations Disrupted by Australia Covid-19 Surge -- Update
18/01/22 Yahoo BHP flags COVID-19 disruption risk as iron ore output rises 5%
19/01/22 Yahoo BHP Group (BHP) Reports 1% Rise in Iron Ore Production in 1H22
20/01/22 Yahoo BHP Shareholders Back Single Listing as Miner Mulls M&A
27/01/22 Yahoo Emfo, Llc Buys BHP Group, P3 Health Partners Inc, American Express Co, Sells Foresight ...
28/01/22 Yahoo BHP Slips as Quarter-Century Presence on FTSE 100 Draws to Close
31/01/22 Yahoo Havens Advisors Llc Buys BHP Group PLC, CyrusOne Inc, NeoPhotonics Corp, Sells , ,
01/02/22 Yahoo Investors in BHP Group (ASX:BHP) have made a strong return of 147% over the past five years
02/02/22 Yahoo Here's what's next for BHP Petroleum's Houston presence after Woodside deal closes
03/02/22 Yahoo Woodside's acquisition of BHP Petroleum will give Houston office more offshore projects
04/02/22 Yahoo Cannon Global Investment Management, LLC Buys BHP Group, SPDR S&P Regional Banking ETF, ...
07/02/22 Yahoo Formidable Asset Management, LLC Buys Flux Power Holdings Inc, BP PLC, BHP Group, Sells Eaton ...
