import requests
import json
import datetime
from datetime import timedelta


def get_ticker_headlines(ticker, keywords):
    '''
    Function which takes in a stock ticker and a list of keywords, and retrieves all the headlines for 45 weeks containing at least one keyword from the description
    ticker - string
    keywords - array of strings
    returns a txt file containing every headline separated by lines
    '''

    #data start and end for each week
    starting_datetime = datetime.datetime(2021, 4, 1)
    ending_datetime = datetime.datetime(2021, 4, 7)

    with open(ticker + ".txt", 'w') as f:
        #45 weeks worth of data
        for i in range(45):
            #progress counter to give feedback to user
            percentage = round((i / 45) * 100, 1) 
            print("Progress: " + str(percentage) + "%", end="\r")

            #string manipulation to get it into the correct format
            starting_year = starting_datetime.year
            starting_month = starting_datetime.month
            starting_day = starting_datetime.day
            ending_year = ending_datetime.year
            ending_month = ending_datetime.month
            ending_day = ending_datetime.day
            #preprocess dates for use with the API
            if starting_month < 10:
                starting_month = '0' + str(starting_month)
            if starting_day < 10:
                starting_day = '0' + str(starting_day)
            if ending_month < 10:
                ending_month = '0' + str(ending_month)
            if ending_day < 10:
                ending_day = '0' + str(ending_day)
            
            starting_datetime_str = str(starting_year) + '-' + str(starting_month) + '-' + str(starting_day)
            ending_datetime_str = str(ending_year) + '-' + str(ending_month) + '-' + str(ending_day)

            #add on seven days for next iteration
            starting_datetime = starting_datetime + timedelta(days=7)
            ending_datetime = ending_datetime + timedelta(days=7)
            
            #API request for all headlines
            req = requests.get('https://finnhub.io/api/v1/company-news?symbol=' + ticker + '&from=' + starting_datetime_str + '&to=' + ending_datetime_str + '&token=c55i7j2ad3icdhg10b60')
            if req.status_code != 200:
                print("Error: Bad request to Finnhub API. Error code: " + requests.status_code)
                continue
            # Iterate through each entry and get headline
            req_json = json.loads(req.text)
            #filter sources
            accepted_sources = ['yahoo', 'marketwatch', 'benzinga', 'dowjones', 'seekingalpha']
            
            req_json.reverse()
            for json_element in req_json:
                #we need three elements from the json request: the source, the date and the headline
                source = json_element["source"]
                if source.lower() not in accepted_sources: #filter out unknown sources
                    continue
                date_time = json_element["datetime"] # make headlines more relevant by only including ones which have the keywords on there
                headline  = json_element["headline"]
                keywords_in_headline = False
                headline_array = headline.split()
                #make sure there's a keyword in the headlines to keep it relevant. If not, then ignore it and move to the next headline
                for keyword in keywords:
                    if keyword in headline_array:
                        keywords_in_headline = True
                        break
                if keywords_in_headline == False:
                    continue
                #datetime from json given as a timestamp so convert to dd/mm/yyyy
                date_time_obj = datetime.datetime.fromtimestamp(date_time)
                date_time = date_time_obj.strftime('%d/%m/%y')
                #write line to text file with all of the data
                f.write(str(date_time) + " " + str(source) + " " + str(headline) + "\n")

def get_ticker_headline_count(ticker_file):
    with open(ticker_file, 'r') as f:
        headline_count = f.readlines()
        return len(headline_count)
        